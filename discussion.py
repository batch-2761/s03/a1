# Python has several structures to store collections or multiple items in a single variable
# List, Dictionary, Tuples(), Set{}
# Tuples - ("Math", "Science", "English")
# Set - {"Math", "Science", "English"}

# [Section] Lists
# Lists are similar to arrays of JS in a sense that they can contain a collection of data
# To create a list, the square bracket([]) is used

names = ["John", "George", "Ringo"] #String List
programs = ["developer career", "pi-shape", "short courses"] #string list

truth_variables = [True, False, True, True, False] #boolean list

# A list can contain elements with different data types:
sample_list = ["Apple", 3, False, "Potato", 4, False]

#Note: Problems may arise if you try to use lists with multiple type for something that expects them to be all the same type

# Getting the list size
# The number of elements in a list can be counted  using len() method
print(len(programs))
# Result:
# 3

var_len = len(sample_list)

print(var_len)
# Result:
# 6

# Accessing values/elements in a list
# List can be accessed by providing the index number of the element
# It can be accessed using the negative index

print(names[0])
# Result:
# John

#Access the last item in the list
print(names[-1])
# Result:
# Ringo

print(programs[1])
# Result:
# pi-shape


# Access the whole list
print(programs)
# Result:
# ['developer career', 'pi-shape', 'short courses']

# Access a range of values
print(programs[0:2])
# Result:
# ['developer career', 'pi-shape']

# Updating lists
print(f"Current value: {programs[2]}")

# Update the value
programs[2] = "ShortCourses"
print(f"New value: {programs[2]}")
# Result:
# Current value: short courses
# New value: ShortCourses

# [Section] List Manipulation
# List has methods 
# Adding List Items - the append() method allows teh insert items to a list

programs.append("global")
print(programs)

# Deleting List items - the "del" keyword can be used to delete items in the list

durations = [260, 180, 20] #number list
durations.append(360)
print(durations)
# Result:
# [260, 180, 20, 360]

del durations[-1]
print(durations)
# Result:
# [260, 180, 20]

del durations[0]
print(durations)
# Result:
# [180, 20]

# insert method
durations.insert(0, 100)
print(durations)
# Result:
# [100, 180, 20]

durations.insert(1, 99)
print(durations)
# Result:
# [100, 99, 180, 20]

# Membership checking - the "in" keyword checks if the element is in the list
# returns True or False value

print(20 in durations)
# Result: True

print(500 in durations)
# Result: False

# Sorting lists - The sort() method sorts the list aphanumerically, ascending, by default

durations.sort()
print(durations)
# Result: [20, 99, 100, 180]

programs.sort()
print(programs)
# Result:
# ['ShortCourses', 'developer career', 'global', 'pi-shape']

# Emptying the list - the clear() method is used to empty the contents of the list

test_list = [1,3,5,7,9]
print(test_list)

test_list.clear()
print(test_list)
# Result:
# [1, 3, 5, 7, 9]
# []

# [Section] Dictionaries are used to store data values in key:value pairs. This is similar to objects in Js/Javascript
# A dictionary is a collection which is order, changeable and does not allow duplicates
# To create a dictionary, the curly braces ({}) is used and the key: value pairs are denoted with (key: value)

person1 = {
	"name" : "Brandon",
	"age" : 28,
	"occupation" : "student",
	"isEnrolled" : True,
	"subjects" : ["Python", "SQL", "Django"]
}

# To get the number of key-value pairs on dictionary, the len() method
print(len(person1))
# Result: 5

# Accessing values in dictionary
# To get item in the dictionary, the key name can be referred using square bracket([])
print(person1["name"])
# Result: Brandon

# The keys() method will return a list of all the keys in the dictionary
print(person1.keys())
# Result:
# dict_keys(['name', 'age', 'occupation', 'isEnrolled', 'subjects'])


# the values() method will return a lsit of all the values in the dictionary
print(person1.values())
# Result:
# dict_values(['Brandon', 28, 'student', True, ['Python', 'SQL', 'Django']])

# The items() method will return each item in the dictionary as key-value pair in a list
print(person1)
print(person1.items())
# Result:
# {'name': 'Brandon', 'age': 28, 'occupation': 'student', 'isEnrolled': True, 'subjects': ['Python', 'SQL', 'Django']}
# dict_items([('name', 'Brandon'), ('age', 28), ('occupation', 'student'), ('isEnrolled', True), ('subjects', ['Python', 'SQL', 'Django'])])

# Adding key-value pairs cna be done with either a new index key and assigning a value or the update method
# index key
person1["nationality"] = "Filipino"
print(person1)
# update method

person1.update({"fav_food": "Sinigang"})
print(person1)

# Deleting entries- it can be done using the pop() method and the del keyword
# using pop method
person1.pop("name")
print(person1)

# using the del keyword
del person1["nationality"]
print(person1)

# The clear() method empties the dictionary
person2 = {
	"name" : "John",
	"age" :18
}

# person2.clear()
print(person2)

# Looping through dictionary

for prop in person2:
	print(f'The value of {prop} is {person2[prop]}')

# Nested dictionaries - dictionaries can be nested inside each other
person3 = {
	"name": "Monika",
	"age" : 20,
	"occupation": "poet",
	"isEnrolled": True,
	"subjects" : ["Python", "SQL", "Django"]
}

print(person3["subjects"][0])
print(person3["subjects"][-1])

student1 = "student1"
student2 = "student2"
class_room = {
	student1 : person1,
	student2 : person3
}
print(class_room["student2"]["subjects"][1])


room1 = "Room 1"
room2 = "Room 2"

rooms = {
	room1: room1,
	room2: room2
}

print(rooms)

# [Section] Functions
# Functions are block of codes that runs when called.
# A function can be used to get inputs, process inputs and return inputs
# The "def" keyword is used to create a function. The syntax:
# def <function_name>()

# defines a dunction called my_greeting
def my_greeting():
	print("Hello user!")

# Calling or Invoking a fuction - To use a function, just specify the function name and provide the value/values needed if there are.
my_greeting()


# Parameters can be added to functions to have more control to what the inputs for the function will be


def greet_user(username):
	print(f"Hello, {username}")

# Arguments are the vlues that are submitted to the parameters
greet_user("Chris")

# Return statements - the "return" keyword allow functions to return values

def addition(num1,num2):
	return num1 + num2

sum = addition(5,9)
print(f"The sum is {sum}!")

# [Section] Lambda Functions
# A lambda function is a small anonymous function that can be used for callbacks
# It is just like any normal python function, except that it has no name when defining it, and it is contained in one of code


# A lambda funciton can take any number of arguments but can only have one expression

greeting = lambda person: f"Hello {person}"

print(greeting("Elsie"))

mult = lambda a, b : a * b

print(mult(5,6))

# [Section] Classes
# Classes would serve as blueprints to describe the concept of objects
# Each Object has characteristics (properties) and behaviors (method)

# To create a Class, the "class" keyword is used along with the class name that starts with an uppercase character


class Car():
	# properties that all car Car objects must have are defined in a method called init

	# initializing the properties of our function
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		# propeties that are hard coded
		self.fuel = "Gasoline"
		self.fuel_level = 0

	# method
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print("Filling up the fuel tank . . .")
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")

	def drive(self, distance):
		print(f"The car has driven {distance} kilometers!")
		self.fuel_level = self.fuel_level - distance
		print(f"The fuel left: {self.fuel_level}")

# intantiate a class
new_car = Car("Nissan", "GT-R", 2019)

print(new_car.brand)
print(new_car.model)
print(new_car.year_of_make)

print(new_car)
# to invoke a method
new_car.fill_fuel()
print(new_car.fuel_level)

new_car.drive(50)


